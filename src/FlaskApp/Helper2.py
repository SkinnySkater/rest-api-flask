from datetime import tzinfo, timedelta, datetime

"""
	inner class to format the created_at index timestamp
	set Timezone to 60 min --> 1 hour.
	and add the additional 'T' ISO norm
"""
class TZ(tzinfo):
	def utcoffset(self, dt):
		return timedelta(minutes=60)

def transform_date(date):
	res = datetime(date.year, date.month, date.day, date.hour, date.minute, date.second, tzinfo=TZ()).isoformat()
	return res

def format_mailer_json(req_data, lang):
	req_resp = {
			'code' : 200,
			'message' : "success",
			"datas" : {
					'langs' : lang,
					'id' : req_data.get('id'),
					'slug' : req_data.get('slug'),
					'name' : req_data.get('name'),
					'description' : req_data.get('description'),
					'creator' : {
							'id' : req_data.get('id'),
							'username' : req_data.get('username')
					},
					'created_at' : transform_date(req_data.get('created_at'))
				}
		}
	return req_resp

def check_domain(cur, name):
	query = "SELECT domain.id FROM domain WHERE domain.name = %s"
	cur.execute(query, name)
	res = cur.fetchall()
	if (len(res) > 0):
		return True
	else:
		return False

def check_user(cur, name, pwd):
	query = "SELECT `domain`.`user_id`\
			FROM `domain`\
			LEFT JOIN `user` ON `domain`.`user_id` = `user`.`id` \
			WHERE `user`.`password` = %s AND `domain`.`name` = %s "
	cur.execute(query, (pwd, name))
	trans_id = cur.fetchall()
	if (len(trans_id) > 0):
		return True
	else:
		return False