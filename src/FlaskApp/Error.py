from flask import Flask
from flask import jsonify

app = Flask(__name__)

@app.errorhandler(400)
def unknown_ext(code, message):
	req_resp = {}
	if (code != 404):
		req_resp['code'] = code
		req_resp['message'] = message
		req_resp['datas'] = ""
		res = jsonify(req_resp)
		res.status_code = code
		return res, code
	else:
		req_resp['code'] = code
		req_resp['message'] = message
		res = jsonify(req_resp)
		res.status_code = code
		return res, code