# -*- coding: utf-8 -*-

from flask import Flask
from flask import jsonify
from flask import Flask, render_template, request
from flask import Response
import json
import sys
from Error import *
from Config import *
from pprint import pprint
from datetime import tzinfo, timedelta, datetime

app = Flask(__name__)

conf = Config(app)
app = conf.app
conn = conf.conn

@app.route("/api/domains.<json>", methods=['GET'])
def domains(json):
	if (str(json) == "json"):
		cur = conn.cursor()
		query = "SELECT `domain`.`id`, `domain`.`slug`, `domain`.`name`, `domain`.`description` FROM `domain`"
		cur.execute(query)

		columns = cur.description
		result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
		req_data = result
		req_resp = {}
		req_resp['code'] = 200
		req_resp['message'] = "success"
		req_resp['datas'] = req_data
		
		res = jsonify(req_resp)
		res.status_code = 200
		return res
	return unknown_ext(400, "bad request")

"""
	inner class to format the created_at index timestamp
	set Timezone to 60 min --> 1 hour.
	and add the additional 'T' ISO norm
"""
class TZ(tzinfo):
	def utcoffset(self, dt): return timedelta(minutes=60)

def transform_date(date):
	res = datetime(date.year, date.month, date.day, date.hour, date.minute, date.second, tzinfo=TZ()).isoformat()
	return res

def format_mailer_json(req_data, lang):
	req_resp = {
			'code' : 200,
			'message' : "success",
			"datas" : {
					'langs' : lang,
					'id' : req_data.get('id'),
					'slug' : req_data.get('slug'),
					'name' : req_data.get('name'),
					'description' : req_data.get('description'),
					'creator' : {
							'id' : req_data.get('id'),
							'username' : req_data.get('username')
					},
					'created_at' : transform_date(req_data.get('created_at'))
				}
		}
	return req_resp

@app.route("/api/domains/<name>.<json>", methods=['GET'])
def mailer(json, name):
	if (str(json) == "json"):
		cur = conn.cursor()
		query = "SELECT DISTINCT domain.id, domain.user_id, domain.name, domain.description, domain.slug, domain.created_at, user.username\
				FROM domain\
				LEFT JOIN user ON domain.user_id = user.id\
				WHERE domain.name = %s"
		query_lang = "SELECT domain_lang.lang_id\
						FROM domain\
						LEFT JOIN user ON domain.user_id = user.id\
						LEFT JOIN domain_lang ON domain.id = domain_lang.domain_id"
		cur.execute(query_lang)
		lang = []
		for x in cur.fetchall():
			lang.extend(x)
		lang.remove(None)
		cur.execute(query, name)
		columns = cur.description
		result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
		if (len(result) == 0):
			return unknown_ext(400, "bad request")
		req_data = result[0]
		req_resp = format_mailer_json(req_data, lang)
		res = jsonify(req_resp)
		res.status_code = 200
		return res
	return unknown_ext(400, "bad request")

@app.errorhandler(404)
def pageNotFound(error):
	code = 404
	req_resp = {}
	req_resp['code'] = code
	req_resp['message'] = "not found"
	res = jsonify(req_resp)
	res.status_code = code
	return res, code

if __name__ == "__main__":
	app.config['JSON_SORT_KEYS'] = False
	app.run('0.0.0.0', port=int(sys.argv[1]), debug=True)
