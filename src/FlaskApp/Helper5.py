from flask import jsonify
from Error import *
from Helper3 import custom_error, get_lang_id, add_new_trans, find_between
from Helper2 import transform_date
from datetime import datetime
"""
		P U T
"""

def get_trans_info(cur, id_):
	query = "SELECT `domain`.`name`, `translation_to_lang`.`translation_id`, `translation`.`code`, GROUP_CONCAT(`translation_to_lang`.`lang_id`, ' : ', `translation_to_lang`.`trans`) as langs_trad\
		FROM `domain` LEFT JOIN `translation` ON `domain`.`id` = `translation`.`domain_id` LEFT JOIN `translation_to_lang` ON `translation`.`id` = `translation_to_lang`.`translation_id`\
		WHERE `translation_to_lang`.`translation_id` = %s GROUP BY `translation`.`id`"
	cur.execute(query, id_)
	res = cur.fetchall()
	if (len(res) > 0):
		return res[0]
	else:
		return False

def update_trans(conn, cur, k, v, id):
	query = "UPDATE `translation_to_lang` SET `trans`=%s WHERE `translation_id` = %s AND `lang_id` = %s"
	"""res = get_trans_id(cur, id)
	if (k not in res):
		return False"""
	try:
		cur.execute(query, (v, id, k))
		conn.commit()
		return True
	except Exception as e:
		print("MDR  ->" + e)
		conn.rollback()
		return False

def get_trans_id(cur, id):
	query = "SELECT `lang_id` FROM `translation_to_lang` WHERE `translation_id` = %s"
	cur.execute(query, id)
	res = cur.fetchall()[0]
	return res

def check_lid(cur, domain, key):
	query = "SELECT `domain_lang`.`lang_id`\
	FROM `domain_lang`\
	LEFT JOIN `domain` ON `domain_lang`.`domain_id` = `domain`.`id`\
	WHERE `domain`.`name` = %s AND `domain_lang`.`lang_id` = %s"
	cur.execute(query, (domain, key))
	res = cur.fetchall()
	if (len(res) > 0):
		return True
	return False

def put_trans(conn, args, headers, cur, domain, id_):
	code = None
	info = get_trans_info(cur, id_)
	if (info == False or len(info) < 4):
		return custom_error(400, "bad request")
	domain_name = info[0]
	id_data = info[1]
	code = info[2]
	trans_rep = info[3].split(',')
	if (id_data != id_ ):
		return custom_error(404, "Not Found")
	if (domain_name != domain):
		return custom_error(400, "bad request")
	for key, value in dict(args).items():
		k = find_between(str(key), '[', ']')
		#check if the letter belongs to the domain's language
		if (not check_lid(cur, domain, k)):
			return custom_error(400, "Not Found")
		v = value[0]#.replace("+", " ")
		if (update_trans(conn, cur, k, v, id_) == False):
			if (add_new_trans(conn, cur, k, v, id_) == False):
				return custom_error(400, "Not Found")
	list_lang_id = [x[0] for x in get_lang_id(cur, domain)]
	all_lg = get_trans_id(cur, id_)
	print("---->", list_lang_id, all_lg)
	for x in list_lang_id:
		if (x not in all_lg):
			if (add_new_trans(conn, cur, x, code, id_) == False):
				return custom_error(400, "Not Found")
	#Get all the matching lang_id and translation into dictionary
	dic_trans_res = dict(map(str, x.split(':')) for x in get_trans_info(cur, id_)[3].split(','))
	print(dic_trans_res)
	req_resp = {
		'code' : 200,
		'message' : "success",
		"datas" : {
			'trans' : dic_trans_res,
			'id' : id_,
			'code' : code
		}
	}
	res = jsonify(req_resp)
	res.status_code = 200
	return res


"""
		D E L E T E
"""

def delete_id(conn, cur, id):
	query1 = "DELETE FROM `translation_to_lang` WHERE `translation_id`=%s;"
	query2 = "DELETE FROM `translation` WHERE `id`=%s;"
	try:
		cur.execute(query1, id)
		cur.execute(query2, id)
		conn.commit()
	except Exception as e:
		print(e)
		conn.rollback()


def del_trans(conn, cur, domain, id):
	info = get_trans_info(cur, id)
	if (info == False or len(info) < 4):
		return custom_error(400, "Not Found")
	domain_name = info[0]
	id_data = info[1]
	if (id_data != id):
		return custom_error(400, "Not Found")
	if (domain_name != domain):
		return custom_error(400, "bad request")
	delete_id(conn, cur, id)
	req_resp = {
		'code' : 200,
		'message' : "success",
		"datas" : {
			'id' : id,
		}
	}
	res = jsonify(req_resp)
	res.status_code = 200
	return res


"""
		F I L T E R
"""

def filter(conn, cur, code, domain):
	code = "%" + code + "%"
	query = "SELECT `domain`.`name`, `translation_to_lang`.`translation_id`, `translation`.`code`\
	,  GROUP_CONCAT(`translation_to_lang`.`lang_id`, ' : ', `translation_to_lang`.`trans`) as langs_trad\
	FROM `domain`\
	LEFT JOIN `translation` ON `domain`.`id` = `translation`.`domain_id` \
	LEFT JOIN `translation_to_lang` ON `translation`.`id` = `translation_to_lang`.`translation_id`\
	WHERE `translation`.`code` LIKE %s AND `domain`.`name` = %s\
	GROUP BY   `translation`.`id`"
	cur.execute(query, (str(code), domain))
	res = cur.fetchall()
	data = {}
	datas = []
	for elt in res:
		d = {}
		#load all id for the current id
		all_lg = [x[0].split(':')[0] for x in get_lang_id(cur, domain)]
		current_lg = [x.split(':')[0].split()[0] for x in elt[3].split(',')]
		#remove all the existing translation
		for n in current_lg:
			if (n in all_lg):
				all_lg.remove(n)
		#add the missing data
		new_tr_list = elt[3].split(',')
		for k in all_lg:
			new_tr_list.append(k + ':' + elt[2])
			add_new_trans(conn, cur, k, elt[2], elt[1])
		d['trans'] = dict(map(str, x.split(':')) for x in new_tr_list)
		d['id'] = elt[1]
		d['code'] = elt[2]
		datas.append(d)
	data['code'] = 200
	data['message'] = "success"
	data['datas'] = datas
	res = jsonify(data)
	res.status_code = 200
	return res


"""
	STEP 8  ---> G E T 
"""

def get_domain_details(cur, domain, pwd):
	query = "SELECT `domain`.`id`, `domain`.`slug`, `domain`.`name`, `domain`.`description`, `user`.`id`\
			, `user`.`username`, `user`.`email`, `domain`.`created_at`,  GROUP_CONCAT(`domain_lang`.`lang_id`, ',')\
			FROM `domain`\
			LEFT JOIN `user` ON `domain`.`user_id` = `user`.`id` \
			LEFT JOIN `domain_lang` ON `domain`.`id` = `domain_lang`.`domain_id`\
			WHERE `user`.`password` = %s AND `domain`.`name` = %s\
			GROUP BY `domain`.`id`"
	cur.execute(query, (pwd, domain))
	res = cur.fetchall()
	data = {}
	datas = []
	data['code'] = 200
	data['message'] = "success"
	for x in res:
		d = {}
		creator = {}
		lang_l = x[8].replace(",,", " ")[:-1].split()
		d['langs'] = lang_l
		d['id'] = x[0]
		d['slug'] = x[1]
		d['name'] = x[2]
		d['description'] = x[3]
		creator['id'] = x[4]
		creator['username'] = x[5]
		creator['email'] = x[6]
		d['creator'] = creator
		d['created_at'] = transform_date(x[7])
		datas.append(d)
	data['datas'] = datas
	result = jsonify(data)
	result.status_code = 200
	return result


"""
	STEP 9  ---> P O S T
"""
def get_user_id(cur, pwd):
	query = "SELECT `user`.`id` FROM `user` WHERE `user`.`password` = %s"
	cur.execute(query, pwd)
	res = cur.fetchall()
	return res[0]

def get_new_domain_id(cur, name):
	query = "SELECT `domain`.`id` FROM `domain` WHERE `domain`.`name` = %s"
	cur.execute(query, name)
	res = cur.fetchall()
	return res[0]

def insert_domain(conn, cur, pwd, args):
	uid = get_user_id(cur, pwd)
	name = args.get('name').replace("+", "-")
	desc = args.get('description').replace("+", " ")
	date = datetime.now()
	insert_query = "INSERT INTO domain (user_id, name, description, slug, created_at)\
					VALUES (%s, %s, %s, %s, %s)"
	try:
		cur.execute(insert_query, (uid, name, desc, name, date))
		conn.commit()
		return True
	except Exception as e:
		print(e)
		conn.rollback()
		return False

def check_laguage(cur, args):
	query = "SELECT `code` FROM `lang`"
	cur.execute(query)
	lg = [x[0] for x in cur.fetchall()]
	n_lg = []
	for k, v in dict(args).items():
		if (k == 'lang[]'):
			n_lg = [x for x in v]
	if (len(n_lg) == 0):
		return False
	for x in n_lg:
		if (len(x) == 0 or x not in lg):
			return False
	return n_lg

def insert_dm_lg(conn, cur, langs):
	query = "SELECT `id` FROM `domain` ORDER BY `id` DESC LIMIT 1"
	cur.execute(query)
	d_id = cur.fetchall()[0]
	query = "INSERT INTO domain_lang VALUES (%s, %s)"
	for n in langs:
		try:
			cur.execute(query, (d_id, n))
			conn.commit()
		except Exception as e:
			conn.rollback()
			return False
	return True


def post_new_domain(conn, cur, pwd, args):
	#Check languages
	langs = check_laguage(cur, args)
	if (langs ==  False):
		print("titi")
		return custom_error(400, "bad request")
	if (not insert_domain(conn, cur, pwd, args)):
		print("tito")
		return custom_error(400, "bad request")
	#add languages id 
	if (not insert_dm_lg(conn, cur, langs)):
		return custom_error(400, "bad request")
	print("tutu")
	#did = get_new_domain_id(cur, args.get('name').replace("+", "-"))
	query = "SELECT `domain`.`id`, `domain`.`slug`, `domain`.`name`, `domain`.`description`, `user`.`id`\
			, `user`.`username`, `user`.`email`, `domain`.`created_at`,  GROUP_CONCAT(`domain_lang`.`lang_id`, ',')\
			FROM `domain`\
			LEFT JOIN `user` ON `domain`.`user_id` = `user`.`id` \
			LEFT JOIN `domain_lang` ON `domain`.`id` = `domain_lang`.`domain_id`\
			WHERE `user`.`password` = %s AND `domain`.`name` = %s\
			GROUP BY `domain`.`id`"
	cur.execute(query, (pwd, args.get('name')))
	res = cur.fetchall()
	data = {}
	datas = []
	data['code'] = 201
	data['message'] = "success"
	for x in res:
		d = {}
		creator = {}
		lang_l = x[8].replace(",,", " ")[:-1].split()
		d['langs'] = lang_l
		d['id'] = x[0]
		d['slug'] = x[1]
		d['name'] = x[2]
		d['description'] = x[3]
		creator['id'] = x[4]
		creator['username'] = x[5]
		creator['email'] = x[6]
		d['creator'] = creator
		d['created_at'] = transform_date(x[7])
		datas.append(d)
	data['datas'] = datas
	result = jsonify(data)
	result.status_code = 200
	return result