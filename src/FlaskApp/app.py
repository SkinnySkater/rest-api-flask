# -*- coding: utf-8 -*-

from flask import Flask
from flask import jsonify
from flask import Flask, render_template, request
from flask import Response
import json
import sys
from Error import *
from Config import *
from pprint import pprint

from Helper2 import *
from Helper3 import *
from Helper5 import *

import logging

app = Flask(__name__)

conf = Config(app)
app = conf.app
conn = conf.conn

file_handler = logging.FileHandler('app.log')
app.logger.addHandler(file_handler)
app.logger.setLevel(logging.INFO)

@app.route("/api/domains.<json>", methods=['GET', 'POST'])
def domains(json):
	cur = conn.cursor()
	if (str(json) == "json" and request.method == "GET"):
		query = "SELECT `domain`.`id`, `domain`.`slug`, `domain`.`name`, `domain`.`description` FROM `domain`"
		cur.execute(query)

		columns = cur.description
		result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
		req_data = result
		req_resp = {}
		req_resp['code'] = 200
		req_resp['message'] = "success"
		req_resp['datas'] = req_data
		
		res = jsonify(req_resp)
		res.status_code = 200
		return res
	if (str(json) == "json" and request.method == "POST"):
		return post_new_domain(conn, cur, request.headers.get('Authorization'), request.form)
	return unknown_ext(400, "bad request")



@app.route("/api/domains/<name>.<json>", methods=['GET'])
def mailer(json, name):
	if (str(json) == "json"):
		cur = conn.cursor()
		if ("Authorization" in request.headers):
			if (not auth_check(cur, request.headers)):
				return custom_error(401, "Unauthorized")
			if (not auth_domain_user(cur, name, request.headers)):
				return custom_error(403, "Forbidden")
			if (not check_domain(cur, name)):
				return unknown_ext(400, "Not Found")
			return get_domain_details(cur, name,  request.headers.get('Authorization'))
		else:
			if (not check_domain(cur, name)):
				return unknown_ext(404, "Not Found")
			query = "SELECT DISTINCT domain.id, domain.user_id, domain.name, domain.description, domain.slug, domain.created_at, user.username\
					FROM domain\
					LEFT JOIN user ON domain.user_id = user.id\
					WHERE domain.name = %s"
			query_lang = "SELECT domain_lang.lang_id\
							FROM domain\
							LEFT JOIN user ON domain.user_id = user.id\
							LEFT JOIN domain_lang ON domain.id = domain_lang.domain_id"
			cur.execute(query_lang)
			lang = []
			for x in cur.fetchall():
				lang.extend(x)
			lang.remove(None)
			cur.execute(query, name)
			columns = cur.description
			result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
			if (len(result) == 0):
				return unknown_ext(400, "bad request")
			req_data = result[0]
			req_resp = format_mailer_json(req_data, lang)
			res = jsonify(req_resp)
			res.status_code = 200
			return res
	return unknown_ext(400, "bad request")



@app.route("/api/domains/<name>/translations.<json>", methods=['GET', 'POST'])
def translation(json, name):
	cur = conn.cursor()
	if (str(json) == "json" and request.method == "GET"):
		if (len(request.args) == 1 and 'code' in request.args and len(request.args.get('code')) > 0):
			return filter(conn, cur, request.args.get('code'), name)
		elif (len(request.args) == 0):
			query = "SELECT `domain`.`name`, `translation_to_lang`.`translation_id`, `translation`.`code`\
			,  GROUP_CONCAT(`translation_to_lang`.`lang_id`, ' : ', `translation_to_lang`.`trans`) as langs_trad\
			FROM `domain`\
			LEFT JOIN `translation` ON `domain`.`id` = `translation`.`domain_id`\
			LEFT JOIN `translation_to_lang` ON `translation`.`id` = `translation_to_lang`.`translation_id` \
			WHERE `domain`.`name` = %s\
			GROUP BY   `translation`.`id`"
			cur.execute(query, name)
			columns = cur.description
			result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
			req_data = []
			for x in result:
				req_data.append(step_3(x, cur, name))
			req_resp = {
				'code' : 200,
				'message' : "success",
				"datas" : req_data
			}
			res = jsonify(req_resp)
			res.status_code = 200
			return res
	if (str(json) == "json" and "POST" in request.method):
		app.logger.info('body------> ' + str(request.data) + ' \nHeaders:-----> ' + str(request.headers) + '\n args:-----> ' + str(request.args) + '\n form:---->' + str(request.form))
		if ('application/x-www-form-urlencoded' != request.headers.get('Content-Type')):
			return custom_error(400, "bad request")
		if ("Authorization" not in request.headers or not auth_check(conn.cursor(), request.headers)):
			return custom_error(401, "Unauthorized")
		if (not auth_domain_user(cur, name, request.headers)):
			return custom_error(403, "Forbidden")
		return post_translation(conn, request.form, request.headers, conn.cursor(), name)
	return custom_error(400, "bad request")


@app.route("/api/domains/<domain>/translations/<int:id>.<json>", methods=['PUT', 'DELETE'])
def put_translation(json, domain, id):
	if (str(json) == "json" and "PUT" in request.method):
		if ('application/x-www-form-urlencoded' != request.headers.get('Content-Type')):
			return custom_error(400, "bad request")
		if ("Authorization" not in request.headers or not auth_check(conn.cursor(), request.headers)):
			return custom_error(401, "Unauthorized")
		if (not auth_domain_user(conn.cursor(), domain, request.headers)):
			return custom_error(403, "Forbidden")
		return put_trans(conn, request.form, request.headers, conn.cursor(), domain, id)
	if (str(json) == "json" and "DELETE" in request.method):
		if (not check_domain(conn.cursor(), domain)):
			return custom_error(400, "bad request")
		if ("Authorization" not in request.headers or not auth_check(conn.cursor(), request.headers)):
			return custom_error(401, "Unauthorized")
		if (not auth_domain_user(conn.cursor(), domain, request.headers)):
			return custom_error(403, "Forbidden")
		return del_trans(conn, conn.cursor(), domain, id)
	return custom_error(400, "bad request")


@app.errorhandler(404)
def pageNotFound(error):
	code = 404
	req_resp = {}
	req_resp['code'] = code
	req_resp['message'] = "not found"
	res = jsonify(req_resp)
	res.status_code = code
	return res, code

if __name__ == "__main__":
	app.config['JSON_SORT_KEYS'] = False
	app.run('0.0.0.0', port=int(sys.argv[1]), debug=True)
