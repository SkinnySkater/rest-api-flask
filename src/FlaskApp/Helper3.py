from flask import jsonify
from Error import *


def custom_error(code, message):
	req_resp = {}
	if (code == 401 or code == 403 or code == 404):
		req_resp['code'] = code
		req_resp['message'] = message
		res = jsonify(req_resp)
		res.status_code = code
		return res, code
	else:
		req_resp['code'] = code
		req_resp['message'] = message
		req_resp['datas'] = []
		res = jsonify(req_resp)
		res.status_code = code
		return res, code

"""
		G E T
"""

def get_lang_domain(cur, domain_name):
	query_id = "SELECT domain.id FROM domain WHERE domain.name = %s"
	query_lang = "SELECT `domain_lang`.`lang_id` FROM `domain_lang` WHERE `domain_lang`.`domain_id` = %s"
	cur.execute(query_id, domain_name)
	id_ = cur.fetchall()
	cur.execute(query_lang, id_[0][0])
	lang = cur.fetchall()
	return lang

def step_3(x, cur, name):
	traduc = x.get('langs_trad')
	lang = get_lang_domain(cur, name)
	for l in lang:
		if (l[0] not in x.get('langs_trad')):
			traduc += ',' + l[0] + " : " + x.get('code')
	a = traduc.split(',')
	data = {
		'trans' : {x.split(':')[0].replace(" ", ""): x.split(': ')[1] for x in a},
		'id' : x.get('translation_id'),
		'code' : x.get('code')
	}
	return data


def find_between(s, start, end):
  return (s.split(start))[1].split(end)[0]


"""
		P O S T
"""

def add_tr(conn, cur, name, code):
	last_id = ""
	query0 = "SELECT id FROM domain WHERE name = %s"
	query1 = "INSERT INTO translation (domain_id, code) VALUES (%s, %s)"
	query3 = "SELECT `id` FROM `translation` ORDER BY `id` DESC LIMIT 1"
	try:
		cur.execute(query0, name)
		d_id = cur.fetchall()[0]
		cur.execute(query1, (d_id, code))
		conn.commit()
		cur.execute(query3)
		last_id = cur.fetchall()[0]
	except Exception as e:
		conn.rollback()
		print(e)
		return False
	finally:
		print(last_id[0])
		return last_id[0]

def add_new_trans(conn, cur, lang_id, trans, last_id):
	query = "INSERT INTO translation_to_lang  VALUES (%s, %s, %s)"
	try:
		cur.execute(query, (last_id, lang_id, trans))
		conn.commit()
	except Exception as e:
		print(e)
		print("mmdrrrrrrrrrrrrrrrrrr")
		conn.rollback()
		return False
	finally:
		return True


def post_translation(conn, args, headers, cur, name):
	if ('code' not in args):
		return custom_error(400, "bad request")
	code = args.get('code')
	if (len(code) == 0):
		return custom_error(400, "bad request")
	if (code_exists(cur, code)):
		return custom_error(400, "bad request")
	#Insert the code into the database table : 'translation'
	trans = []
	last_id = add_tr(conn, cur, name, code)
	list_lang_id = [x[0] for x in get_lang_id(cur, name)]
	if (last_id == False):
		#undo the previous add here
		return custom_error(400, "Conflict")
	for key, value in dict(args).items():
		if ('code' not in str(key)):
			try:
				k = find_between(str(key), '[', ']')
				v = value[0]#.replace("+", " ")
				list_lang_id.remove(k)
				if (add_new_trans(conn, cur, k, v, last_id) == False):
					return custom_error(400, "Conflict")
				trans.append(k + ':' + v)
			except Exception as e:
				return custom_error(400, "Conflict")
	for elt in list_lang_id:
		trans.append(elt + ':' + code)
		if (add_new_trans(conn, cur, elt, code, last_id) == False):
				return custom_error(400, "Conflict")
	trans_res = dict(map(str, x.split(':')) for x in trans)
	req_resp = {
			'code' : 201,
			'message' : "success",
			"datas" : {
					'trans' : trans_res,
					'id' : last_id,
					'code' : code
				}
		}
	res = jsonify(req_resp)
	res.status_code = 201
	return res, 201

def code_exists(cur, code):
	query = "SELECT `code` FROM `translation` WHERE `translation`.`code` = %s"
	cur.execute(query, code)
	res = cur.fetchall()
	if (len(res) > 0):
		return True
	else:
		return False

def auth_check(cur, args):
	query = "SELECT `user`.`password` FROM `user` WHERE `user`.`password` = %s"
	cur.execute(query, args.get('Authorization'))
	res = cur.fetchall()
	if (len(res) > 0):
		return True
	else:
		return False

def auth_domain_user(cur, name, headers):
	query = "SELECT `user`.`password`, `domain`.`name`\
	FROM `user`\
	LEFT JOIN `domain` ON `user`.`id` = `domain`.`user_id`\
	WHERE `user`.`password` = %s  AND `domain`.`name` = %s"
	cur.execute(query, (headers.get('Authorization'), name))
	res = cur.fetchall()
	if (len(res) > 0):
		return True
	else:
		return False

def get_trans_id(cur, name, pwd):
	query = "SELECT `domain`.`user_id`\
			FROM `domain`\
			LEFT JOIN `user` ON `domain`.`user_id` = `user`.`id` \
			WHERE `user`.`password` = %s AND `domain`.`name` = %s "
	cur.execute(query, (pwd, name))
	trans_id = cur.fetchall()
	if (len(trans_id) > 0):
		return True
	else:
		return False


def get_lang_id(cur, name):
	query = "SELECT `domain_lang`.`lang_id`\
			FROM `domain`\
			LEFT JOIN `user` ON `domain`.`user_id` = `user`.`id`\
			LEFT JOIN `domain_lang` ON `domain`.`id` = `domain_lang`.`domain_id`\
			WHERE `domain`.`name` = %s"
	cur.execute(query, name)
	res = cur.fetchall()
	return res
