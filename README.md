################
# PREREQUISITE #
################
	Python 2.6 >=
	MySQL

################
# INSTALLATION #
################
	(python framework and package)
	sudo apt-get install python-setuptools
	sudo easy_install pip
	sudo pip install flask  (and or pip install flask-mysql)
	sudo pip install flask-restful
	(mysql:)
	apt-get install mysql-server mysql-client libmysqlclient15-dev mysql-common

	(connect to mysql:)
		mysql -u username -p 
	(create database:)
        CREATE DATABASE name; <ENTER>
	(configure database)
	mysql -u username -p database_name < file.sql

	(shut down apache2 or any other process):
	sudo service apache2 stop
	or:
		sudo lsof -n -i :80 | grep LISTEN
		kill PID

################
# RUN THE PROC #
################
	nohup sudo python src/FlaskApp/app.py 80 &
	CHECK: ps -a

################
# ERROR CODE   #
################
{
	"code": 404,
	"message": "not found"
}

{
	"code": 400,
	"message": "error",
	"datas": []
}

#########
#STEP 1:#
#########
	PROTOCOL:	curl -X "GET" /api/domains.json
	JSON :	{
				"code": 200,
				"message": "success",
				"datas": [
					{
						"id": 1,
						"slug": "mailer",
						"name": "mailer",
						"description": "Liste des mails automatisé"
					},
					{
						"id": 2,
						"slug": "documents",
						"name": "documents",
						"description": "un petit teste de documents a traduire."
					}
				]
			}

#########
#STEP 2:#
#########
	PROTOCOL:	curl -X "GET" /api/domains/mailer.json
	{
		"code": 200,
		"message": "success",
		"datas":
		{
			"langs": [
				"EN",
				"FR",
				"PL"
			],
			"id": 1,
			"slug": "mailer",
			"name": "mailer",
			"description": "Liste des mails automatisé",
			"creator":
			{
				"id": 1,
				"username": "lilelulo"
			},
			"created_at": "2018-01-27T00:00:00+01:00"
		}
	}

#########
#STEP 3:#
#########
	PROTOCOL:	curl -X "GET" /api/domains/mailer/translations.json
	{
		"code": 200,
		"message": "success",
		"datas": [
			{
				"trans": {
					"EN": "hello",
					"FR": "bonjour",
					"PL": "__registration__"
				},
				"id": 1,
				"code": "__registration__"
			},
			{
				"trans": {
					"EN": "bye bye",
					"FR": "au revoir",
					"PL": "__bye__"
				},
				"id": 2,
				"code": "__bye__"
			},
			{
				"trans": {
					"EN": "5a6fa30b80f0e - EN",
					"FR": "5a6fa30b80f0e - FR",
					"PL": "_5a6fa30b80f0e__"
				},
				"id": 7,
				"code": "_5a6fa30b80f0e__"
			}
		]
	}


#########
#STEP 4:#
#########
	PROTOCOL:	curl -X "POST"
				-d "code=test_3&trans[FR]=je+suis+un+test+fr&trans[EN]=i+m+a+test+en"
				-H "Content-type: application/x-www-form-urlencoded"
				-H "Authorization: ezygazkfuygezkfjgzkefj"
				/api/domains/mailer/translations.json
	{
		"code": 201,
		"message": "success",
		"datas": {
			"trans": {
				"FR": "je suis un test fr",
				"EN": "i m a test en",
				"PL": "test_3"
			},
			"id": 140,
			"code": "test_3"
		}
	}

#########
#STEP 5:#
#########
	PROTOCOL:	curl -X "PUT"
	-d "trans[PL]=je+parle+pas+polonais"
	-H "Content-type: application/x-www-form-urlencoded"
	-H "Authorization: ezygazkfuygezkfjgzkefj"
	/api/domains/mailer/translations/140.json
	{
		"code": 200,
		"message": "success",
		"datas": {
			"trans": {
				"FR": "azef azef azefaz e",
				"EN": "test",
				"PL": "je parle pas polonais"
			},
			"id": 140,
			"code": "test"
		}
	}

#########
#STEP 6:#
#########
	PROTOCOL:	curl -X "DELETE"
				-H "authorization: ezygazkfuygezkfjgzkefj"
				/api/domains/mailer/translations/140.json
	{
		"code": 200,
		"message": "success",
		"datas": {
			"id": 140
		}
	}

#########
#STEP 7:#
#########
	PROTOCOL:	curl -X "GET"
				/api/domains/mailer/translations.json?code=reg
	{
		"code": 200,
		"message": "success",
		"datas": [
			{
				"trans": {
					"EN": "hello",
					"FR": "bonjour",
					"PL": "__registration__"
				},
				"id": 1,
				"code": "__registration__"
			},
			{
				"trans": {
					"EN": "not possible - EN",
					"FR": "pas possible - FR",
					"PL": "registration_fail"
				},
				"id": 7,
				"code": "registration_fail"
			}
		]
	}

#########
#STEP 8:#
#########
	PROTOCOL:	curl -X "GET"
				-H "Authorization: ezygazkfuygezkfjgzkefj"
				/api/domains/mailer.json
	{
		"code": 200,
		"message": "success",
		"datas": {
			"langs": [
				"EN",
				"FR",
				"PL"
			],
			"id": 1,
			"slug": "mailer",
			"name": "mailer",
			"description": "Liste des mails automatisé",
			"creator": {
				"id": 1,
				"username": "lilelulo",
				"email": "lilelulo@tipeee.com"
			},
			"created_at": "2018-01-27T00:00:00+01:00"
		}
	}

#########
#STEP 9:#
#########
	PROTOCOL:	curl -X "POST"
				-d "name=new+domain&description=short+description&lang[]=EN&lang[]=FR"
				-H "Content-type: application/form-data"
				-H "Authorization: ezygazkfuygezkfjgzkefj"
				/api/domains.json
	{
		"code": 201,
		"message": "success",
		"datas": {
			"langs": [
			"EN",
			"FR"
			],
			"id": 4,
			"slug": "new-domain",
			"name": "new domain",
			"description": "short description",
			"creator": {
				"id": 1,
				"username": "lilelulo",
				"email": "lilelulo@tipeee.com"
			},
			"created_at": "2018-01-27T00:00:00+01:00"
		}
	}

##########
#STEP 10:#
##########
	PROTOCOL:	curl -X "POST"
				-d "name=new+domain&description=short+description&lang[]=EN&lang[]=FR"
				-H "Content-type: application/form-data"
				-H "Authorization: ezygazkfuygezkfjgzkefj"
				/api/domains.json
	{
		"code": 201,
		"message": "success",
		"datas": {
			"langs": [
			"EN",
			"FR"
			],
			"id": 4,
			"slug": "new-domain",
			"name": "new domain",
			"description": "short description",
			"creator": {
				"id": 1,
				"username": "lilelulo",
				"email": "lilelulo@tipeee.com"
			},
			"created_at": "2018-01-27T00:00:00+01:00"
		}
	}

